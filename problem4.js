module.exports=function (inventory) {
  if(!inventory || inventory.length  ==0 || !Array.isArray(inventory)){
    return [];
  }else{
    let carOlderThanYear2000 = [];
    for (let i = 0; i < inventory.length; i++) {
      let car = inventory[i];
      if (car.car_year < 2000) {
        carOlderThanYear2000.push({
          car_make: car.car_make,
          car_model: car.car_model,
          car_year: car.car_year,
          id: car.id,
        });
      }}
    console.log(carOlderThanYear2000);}
  }