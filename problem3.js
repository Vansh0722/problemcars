module.exports = function (inventory) {
  if (!inventory || inventory.length == 0 || !Array.isArray(inventory)) {
    return [];
  } else {
    let carModel = [];
    for (let i = 0; i < inventory.length; i++) {
      let car = inventory[i];
      carModel.push(car.car_model);
    }
    console.log(carModel.sort());
  }
};
