module.exports = function (inventory, id) {
  if (!inventory || inventory.length == 0 || !id || !Array.isArray(inventory)) {
    return [];
  } else {
    let car= {}
  

    for(let i = 0; i < inventory.length; i++){

      car = inventory[i];

      if(car.id === id){
          return car;
      }
  }
    return car
  }
};
